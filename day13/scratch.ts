import {Graphic, splitAt, findReflectionPoint, smudgedSum} from "./day13"


let g = new Graphic();
    g.addRow("##.#.........");
    g.addRow("..#...######.");
    g.addRow("..####......#");
    g.addRow("##....#.##.#.");
    g.addRow("...#...####..");
    g.addRow("..#..#.####.#");
    g.addRow(".......#..#..");
    g.addRow("##.#.########");
    g.addRow("..####......#");
    g.addRow("...##...###..");
    g.addRow("..#.###.##.##");
    g.generateColumns();

    let s = smudgedSum(g);