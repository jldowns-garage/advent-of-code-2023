import * as fs from 'fs';
import * as readline from 'readline';
const file: string = 'day13/input.txt';

class Graphic {
    rowGraphics: string[] = [];
    columnGraphics: string[] = [];

    constructor () {

    }

    addRow(s: string){ this.rowGraphics.push(s); }

    generateColumns() {
        for (let i=0; i<this.rowGraphics[0].length; i++) {
            this.columnGraphics.push(
                this.rowGraphics
                    .map((x)=>x[i])
                    .join("")
                )
        }
    }

    print() {
        for (let r of this.rowGraphics) {
            console.log(r);
        }
    }

}

function splitAt(s: string, i: number): string[] {
    let length = Math.min(i, s.length-i);
    let left = s.slice(i-length,i);
    let right = s.slice(i,i+length);
    return [left, right];
}

function reverseString(str: string): string {
    return str.split("").reverse().join("");
}

var letters = "abcdefghijklmnopqrstuvwxyz";
function findReflectionPoint(s: string[], bannedAnswers: number[] = []) {
    let colors = new Map<string, string>();
    let c = 0;

    // Categorize strings
    var coloredStrings = s.map((x)=>{
        if (colors.has(x)) {
            return colors.get(x)!;
        }
        c++;
        colors.set(x, `${letters[c]}`);
        return letters[c];
    }).join("");

    // find the point
    for (let i=1; i< s.length; i++) {
        var [left, right] = splitAt(coloredStrings, i);
        // console.log(`testing ${left} vs ${right} (${coloredStrings})`)
        if (left == reverseString(right) && !bannedAnswers.includes(i)) {
            return i;
        }
    }

    return -1;
}

function smudgedSum(g: Graphic): number {
    let partialSum = 0;
    let matches = 0;
    let bannedRow = [findReflectionPoint(g.rowGraphics)];
    let bannedColumn = [findReflectionPoint(g.columnGraphics)];
    console.log(`banned row: ${bannedRow} banned col: ${bannedColumn}`)

    for (let r=0;r<g.rowGraphics.length;r++){
        for (let c=0;c<g.columnGraphics.length;c++){
            let newGraphic = new Graphic();
            let rgs = g.rowGraphics.slice();
            if (rgs[r][c] == ".") {
                rgs[r] = rgs[r].slice(0,c) + "#" + rgs[r].slice(c+1,rgs[r].length);
            } else {
                rgs[r] = rgs[r].slice(0,c) + "." + rgs[r].slice(c+1,rgs[r].length);
            }
            newGraphic.rowGraphics = rgs
            newGraphic.generateColumns();
            let cr = findReflectionPoint(newGraphic.columnGraphics, bannedColumn);
            let rr = findReflectionPoint(newGraphic.rowGraphics, bannedRow);
            if (bannedColumn.includes(cr)) { cr = -1; } else {bannedColumn.push(cr);}
            if (bannedRow.includes(rr)) { rr = -1; } else {bannedRow.push(rr);}
            partialSum += (cr==-1)?0:cr;
            partialSum += (rr==-1)?0:rr * 100;
            if (rr != -1 || cr != -1) {
                matches ++;
                console.log(`sumsofar: ${partialSum}`);
            }

            // console.log(" ");
            // newGraphic.print();
            // console.log(`change is at (${r}, ${c})`)
            // console.log(`column reflection point: ${cr}`);
            // console.log(`row reflection point: ${rr}`);
        }
    }

    console.log(`found ${matches} matches. ${(matches!=1)?"<------":""}`)

    return partialSum
}


export {Graphic, splitAt, findReflectionPoint, smudgedSum};



if (require.main === module) {

    var graphics: Graphic[] = [new Graphic()];


    // Create a readable stream for the file
    const fileStream: fs.ReadStream = fs.createReadStream(file);

    // Create an interface for reading the file line by line
    const rl: readline.Interface = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity // Recognize all instances of CR LF ('\r\n') as a single line break.
    });

    // On Line
    rl.on('line', (line: string) => {

        if (line.length < 1) {
            graphics.push(new Graphic());
        } else {
            graphics[graphics.length-1].addRow(line);
        }

    });


    // On End
    rl.on('close', () => {

        graphics.forEach((x)=>x.generateColumns());

        var gIndex = 0;

        let sum = graphics.map((g):number=>{
            return smudgedSum(g);
        })
        
        console.log(sum)
        let sum2 = sum.reduce((a,b)=>a+b);

    console.log(sum2);

    
    });

    // On error
    fileStream.on('error', (error: NodeJS.ErrnoException) => {
    console.error(`Error reading the file: ${error.message}`);
    });

}