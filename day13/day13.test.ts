/* spellchecker: disable */

import {Graphic, splitAt, findReflectionPoint, smudgedSum} from "./day13"

test('Columns generate', () => {

    let g = new Graphic();
    g.addRow("#.##..##.");
    g.addRow("..#.##.#.");
    g.generateColumns();
    expect(g.columnGraphics).toEqual([
        "#.",
        "..",
        "##",
        "#.",
        ".#",
        ".#",
        "#.",
        "##",
        ".."
    ])
});

test("Split at finction", () => {
    let s = "123456789";
    expect(splitAt(s, 1)).toEqual(["1", "2"]);
    expect(splitAt(s, 2)).toEqual(["12", "34"]);
    expect(splitAt(s, 3)).toEqual(["123", "456"]);
    expect(splitAt(s, 4)).toEqual(["1234", "5678"]);
    expect(splitAt(s, 5)).toEqual(["2345", "6789"]);
    expect(splitAt(s, 6)).toEqual(["456", "789"]);
    expect(splitAt(s, 7)).toEqual(["67", "89"]);
    expect(splitAt(s, 8)).toEqual(["8", "9"]);
});

test("Test reflection point", () => {

    var input = [
        "#...##..#",
        "#....#..#",
        "..##..###",
        "#####.##.",
        "#####.##.",
        "..##..###",
        "#....#..#",
    ]

    expect(findReflectionPoint(input)).toEqual(4)
})

test("Integrated test", ()=>{

    var g = new Graphic();
    //////////123456789
    g.addRow("#.##..##.")
    g.addRow("..#.##.#.")
    g.addRow("##......#")
    g.addRow("##......#")
    g.addRow("..#.##.#.")
    g.addRow("..##..##.")
    g.addRow("#.#.##.#.")
    g.generateColumns();
    expect(findReflectionPoint(g.columnGraphics)).toEqual(5);
    expect(findReflectionPoint(g.rowGraphics)).toEqual(-1);

    g = new Graphic();
    g.addRow("#...##..#");
    g.addRow("#....#..#");
    g.addRow("..##..###");
    g.addRow("#####.##.");
    g.addRow("#####.##.");
    g.addRow("..##..###");
    g.addRow("#....#..#");
    g.generateColumns();
    expect(findReflectionPoint(g.columnGraphics)).toEqual(-1);
    expect(findReflectionPoint(g.rowGraphics)).toEqual(4);

    g = new Graphic();
    g.addRow(".#..####...");
    g.addRow("####.##.###");
    g.addRow(".##.#...###");
    g.addRow("##......###");
    g.addRow("#######.#..");
    g.addRow("#######.#..");
    g.addRow("#.......###");
    g.addRow(".##.#...###");
    g.addRow("####.##.###");
    g.addRow(".#..####...");
    g.addRow("#...###....");
    g.addRow(".##....#...");
    g.addRow("###..##.###");
    g.addRow(".##...#..##");
    g.addRow("..##.###.##");
    g.generateColumns();
    console.log("------- Columns ---------")
    expect(findReflectionPoint(g.columnGraphics)).toEqual(10);
    console.log("------- Rows ---------")
    expect(findReflectionPoint(g.rowGraphics)).toEqual(-1);

    g = new Graphic();
    g.addRow("##.#.........")
    g.addRow("..#...######.")
    g.addRow("..####......#")
    g.addRow("##....#.##.#.")
    g.addRow("...#...####..")
    g.addRow("..#..#.####.#")
    g.addRow(".......#..#..")
    g.addRow("##.#.########")
    g.addRow("..####......#")
    g.addRow("...##..####..")
    g.addRow("..#.###.##.##")
    g.generateColumns();
    expect(findReflectionPoint(g.columnGraphics, [1])).toEqual(9);
    expect(findReflectionPoint(g.rowGraphics)).toEqual(-1);


});


test ("Test the smudging", () => {
    let g = new Graphic();
    g.addRow("##.#.........");
    g.addRow("..#...######.");
    g.addRow("..####......#");
    g.addRow("##....#.##.#.");
    g.addRow("...#...####..");
    g.addRow("..#..#.####.#");
    g.addRow(".......#..#..");
    g.addRow("##.#.########");
    g.addRow("..####......#");
    g.addRow("...##...###..");
    g.addRow("..#.###.##.##");
    g.generateColumns();

    let s = smudgedSum(g);

    expect(s).toEqual(9);
    ///
});