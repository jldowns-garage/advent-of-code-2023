import {rotateCW, rollLineLeft, rollLeft, spinMany, spinOnce, loadOf, printMirror} from "./day14-2"


let m: string[] = [];
m.push("O....#....");
m.push("O.OO#....#");
m.push(".....##...");
m.push("OO.#O....O");
m.push(".O.....O#.");
m.push("O.#..O.#.#");
m.push("..O..#O..O");
m.push(".......O..");
m.push("#....###..");
m.push("#OO..#....");



for (let i=1; i<20; i++) {
    m = spinOnce(m)
    console.log(`${i}: ${loadOf(m)}`);
}