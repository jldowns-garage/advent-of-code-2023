/* spellchecker: disable */

import {rotateCW, rollLineLeft, rollLeft, spinMany, spinOnce, loadOf, printMirror} from "./day14-2"

test('Correct rotation', () => {
    let m: string[] = [];
    m.push("abcd");
    m.push("efgh");
    m.push("ijkl");
    m.push("mnop");
    let rotated = [
        "miea",
        "njfb",
        "okgc",
        "plhd",
    ]
    let r = rotateCW(m);
    expect(r).toEqual(rotated);
});

test("Rolling Left", () => {

    let input = "..#.O.O..#O...#.";
    let expectedOutput = "..#.O.O..#O...#.";

    expect(rollLineLeft("..#.O.O..#O...#.")).toEqual("..#....OO#...O#.");
    expect(rollLineLeft("..#.O.O..#O...#")).toEqual("..#....OO#...O#");
    expect(rollLineLeft("O.#.O.O..#O...#")).toEqual(".O#....OO#...O#");
    expect(rollLineLeft("OO#.O.O..#O...#")).toEqual("OO#....OO#...O#");
    expect(rollLineLeft(".OO#.O.O..#O...#")).toEqual(".OO#....OO#...O#");
    expect(rollLineLeft(".OO#.O.O..#O...#.")).toEqual(".OO#....OO#...O#.");
    expect(rollLineLeft(".OO#.O.O..#O...#.O.")).toEqual(".OO#....OO#...O#..O");

});

test("Roll everything left", ()=> {

    let inp = [
        "..#.O.O..#O...#.",
        "..#.O.O..#O...#",
        "O.#.O.O..#O...#",
        "OO#.O.O..#O...#",
        ".OO#.O.O..#O...#",
        ".OO#.O.O..#O...#.",
        ".OO#.O.O..#O...#.O.",
    ]

    let out = [
        "..#....OO#...O#.",
        "..#....OO#...O#",
        ".O#....OO#...O#",
        "OO#....OO#...O#",
        ".OO#....OO#...O#",
        ".OO#....OO#...O#.",
        ".OO#....OO#...O#..O",
    ]

    expect(rollLeft(inp)).toEqual(out);
})

test("Correct Load", () => {
    let m: string[] = [];
    m.push("O....#....");
    m.push("O.OO#....#");
    m.push(".....##...");
    m.push("OO.#O....O");
    m.push(".O.....O#.");
    m.push("O.#..O.#.#");
    m.push("..O..#O..O");
    m.push(".......O..");
    m.push("#....###..");
    m.push("#OO..#....");

    m = rotateCW(m);
    m = rollLeft(m);
    m = rotateCW(m);
    m = rotateCW(m);
    m = rotateCW(m);

    expect(loadOf(m)).toEqual(136)
});

test("Small Spins", () => {

    let m: string[] = [];
    m.push("O....#....");
    m.push("O.OO#....#");
    m.push(".....##...");
    m.push("OO.#O....O");
    m.push(".O.....O#.");
    m.push("O.#..O.#.#");
    m.push("..O..#O..O");
    m.push(".......O..");
    m.push("#....###..");
    m.push("#OO..#....");

    let cycle1Result = [
    ".....#....",
    "....#...O#",
    "...OO##...",
    ".OO#......",
    ".....OOO#.",
    ".O#...O#.#",
    "....O#....",
    "......OOOO",
    "#...O###..",
    "#..OO#....",
    ]

    expect(spinOnce(m)).toEqual(cycle1Result);
    // expect(spinMany(m, 1)).toEqual(cycle1Result);

});