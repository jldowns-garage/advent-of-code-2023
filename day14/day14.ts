const file: string = 'day14/test.txt';
import * as fs from 'fs';
import * as readline from 'readline';

var rollCache = new Map<string, string>();
var bigRollCache = new Map<string, string[][]>();
var spinCache = new Map<string, string[][]>();
var spinCacheIndex = new Map<string, number>();

class Mirror2 {
    lines: string[][] = [];
    spins: number = 0;

    // rows and cols are 1-indexed.
    constructor (
    public row: number,
    public col: number,
    public value: string,
    ) {}

    rowColKey(): string { return `${this.row}_${this.col}`; }
}

class Mirror {

    // things: Thing[] = [];
    things: Map<string, Thing> = new Map<string, Thing>(); // string is in format "row_col"
    maxRow: number = 0;
    maxCol: number = 0;

    addThing(t: Thing) {
        this.things.set(t.rowColKey(), t)
    }

    allThings() {
        return new Array(...this.things.values());
    }

    load() {
        return this.allThings()
            .filter((x)=> x.value == "O")
            .map((x)=> this.maxRow - x.row + 1)
            .reduce((a,b)=>a+b) // sum
    }

    rollNorth() {
        for (var thing of this.allThings()) {
            
            if (thing.value == "#") { continue; }

            while(
                !this.things.has(`${thing.row-1}_${thing.col}`) &&
                thing.row > 1
            ) {
                let oldKey = thing.rowColKey();
                thing.row -= 1;
                // Update Map
                this.things.set(thing.rowColKey(), thing);
                this.things.delete(oldKey);
            }
        }
        this.lines = newLines;
    }

    rollMeLeft() {
        var fingerprint = this.lines.map((x)=>{
            return x.join("");
        }).join("");

        if (bigRollCache.has(fingerprint)) { 
            return bigRollCache.get(fingerprint)!
        }

        for (let i=0; i<this.lines.length; i++) {
            this.lines[i] = rollLeft( this.lines[i].join("") ).split("");
        }

        // Make a 2-level deep copy
        var newLines = this.lines.map(arr => arr.slice());
        bigRollCache.set(fingerprint, newLines);
    }

    load() {
        let sum = 0;
        for (let i=0; i<this.lines.length; i++) {
            sum += (this.lines.length - i) * this.lines[i].filter((x)=>x=="O").length;
        }
        return sum;
    }

    print() {
        for (let l of this.lines) {
            console.log(l.join(""));
        }
    }


    spin(n: number) {
            
        for (let i=0; i<n; i++) {

            this.spins+=1;

            var fingerprint = this.lines.map((x)=>{
                return x.join("");
            }).join("");

            if (spinCache.has(fingerprint)) {
                console.log(`Repeat at ${this.spins} from ${spinCacheIndex.get(fingerprint)}`);
                // this.lines = spinCache.get(fingerprint)!;
                // continue;
            }

        this.rotateCW();
        this.rollMeLeft(); // north
        this.rotateCW();
        this.rollMeLeft(); // west
        this.rotateCW();
        this.rollMeLeft(); // south
        this.rotateCW();
        this.rollMeLeft(); // east


        // Make a 2-level deep copy
        var newLines = this.lines.map(arr => arr.slice());
        spinCache.set(fingerprint, newLines);
        spinCacheIndex.set(fingerprint, this.spins);
        }
    }


    print() {
        for (let r=1; r<=this.maxRow; r++) {
            let s = ""
            for (let c=1; c<=this.maxCol; c++) {
                if (this.things.has(`${r}_${c}`)) {
                    s = s + this.things.get(`${r}_${c}`)?.value!;
                } else {
                    s = s + "."
                }
            }
            console.log(s);
        }
    }
}

function rollLeft(s: string): string {

    if (rollCache.has(s)) { 
        return rollCache.get(s)!;
    }

    let oldS = s;

    // returns a string where the boulders roll left
    let rollables = s.match(/[\\.O]+/g); // [ '.OO', '.O.O..', 'O...', '.O.' ]
    for (let r of rollables!) {
        let os = r.match(/O/g)
        if (os) {
            var osLength = os.length!
        } else {
            osLength = 0;
        }
        s = s.replace(r, ".".repeat(r.length-osLength) + "O".repeat(osLength));
    }

    rollCache.set(oldS, s);

    return s;
}



export {Mirror2, rollLeft};

if (require.main === module) {


    // Create a readable stream for the file
    const fileStream: fs.ReadStream = fs.createReadStream(file);

    // Create an interface for reading the file line by line
    const rl: readline.Interface = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity // Recognize all instances of CR LF ('\r\n') as a single line break.
    });


    var mirror = new Mirror2();

    // On Line
    rl.on('line', (line: string) => {
        var col = 0;
        row ++;
        mirror.maxCol = line.length;
        mirror.maxRow = row;

        for (let t of line) {
            col ++;
            if (t != ".") {
                mirror.addThing(new Thing(row, col, t));
            }
        }

    });

    // On End
    rl.on('close', () => {

        // mirror.print();

        for (let j=0; j<200; j++) {
            if (mirror.load() == 64) {
                console.log(`${j}: ${mirror.load()}`);
            }
                    
            mirror.spin(1);
        }


    
    });

    // On error
    fileStream.on('error', (error: NodeJS.ErrnoException) => {
    console.error(`Error reading the file: ${error.message}`);
    });

}