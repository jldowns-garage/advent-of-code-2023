const file: string = 'day14/input.txt';
import * as fs from 'fs';
import * as readline from 'readline';


function rotateCW(s: string[]): string[] {
    var rotated: string[] = Array(s.length).fill("");

    for (let line=0; line<s.length; line++) {
        for (let char=0; char<s[line].length; char++) {
            rotated[char] = s[line][char] + rotated[char];
        }
    }

    return rotated;
}

function rollLineLeft(s: string): string {

    let oldS = s;

    // returns a string where the boulders roll left
    let rollables = s.match(/[\\.O]+/g); // [ '.OO', '.O.O..', 'O...', '.O.' ]
    for (let r of rollables!) {
        let os = r.match(/O/g)
        if (os) {
            var osLength = os.length!
        } else {
            osLength = 0;
        }
        s = s.replace(r, ".".repeat(r.length-osLength) + "O".repeat(osLength));
    }

    return s;
}

function rollLeft(s: string[]) {
    return s.map((x)=>rollLineLeft(x));
}

function spinOnce(s: string[]): string[] {
    let s1 = s.slice()
    s1 = rotateCW(s1);
    s1 = rollLeft(s1); // north
    s1 = rotateCW(s1);
    s1 = rollLeft(s1); // west
    s1 = rotateCW(s1);
    s1 = rollLeft(s1); // south
    s1 = rotateCW(s1);
    s1 = rollLeft(s1); // east
    return s1;
}

function printMirror(s: string[]) {
    console.log(s.join("\n"));
}

function spinMany(s:string[], n: number): string[] {
    let s1 = s.slice();
    for (let i=0; i<n; i++) {
        s1 = spinOnce(s1);
    }
    return s1;
}

function fingerprint(s: string[]): string {
    return s.join("_");
}

function spinUntilRepeat(s: string[]): number[] {
    var spins = 0;
    var cache = new Map<string, number>(); // fingerprint, spins
    var s1 = s.slice();

    while (true) {
        spins++;
        s1 = spinOnce(s1);
        let f = fingerprint(s1);
        
        if (cache.has(f)) {
            return [cache.get(f)!, Math.max(...cache.values())];
        } else {
            cache.set(f, spins);
        }
    }

}

function loadOf(s: string[]): number {
    let sum = 0;
    for (let lineNumber=0; lineNumber<s.length; lineNumber++) {
        let os = s[lineNumber].match(/O/g);
        sum += (os?os.length:0) * (s.length-lineNumber);
    }

    return sum;
}



export {rotateCW, rollLineLeft, rollLeft, spinOnce, spinMany, loadOf, printMirror}


if (require.main === module) {


    // Create a readable stream for the file
    const fileStream: fs.ReadStream = fs.createReadStream(file);

    // Create an interface for reading the file line by line
    const rl: readline.Interface = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity // Recognize all instances of CR LF ('\r\n') as a single line break.
    });


    var mirror: string[] = [];

    // On Line
    rl.on('line', (line: string) => {

        mirror.push(line);

    });

    // On End
    rl.on('close', () => {

        // mirror.print();

        var [a, b] = spinUntilRepeat(mirror);


        var total = 1000000000;
        var farEnough = (total-a + 1)%(b-a) + a + 1;

        console.log(`${a}, ${b}, far enough: ${farEnough}`);

        var final = spinMany(mirror, farEnough)
        console.log(loadOf(final));

        for (let j=a-2;j<=b+2;j++ ) {
            console.log(`${j}: ${loadOf(spinMany(mirror, j))}`);
        }


    
    });

    // On error
    fileStream.on('error', (error: NodeJS.ErrnoException) => {
    console.error(`Error reading the file: ${error.message}`);
    });

}