const file: string = 'day12/input.txt';
import {possibilities2} from "./day12-2"

// Day12
function sumFunc(a:number,b:number) { return a + b; }

var partials: Map<string, number> = new Map<string, number>();

class SpringRecord {

    public statuses: string[];
    public numericalRecords: number[];
    private previousPossibilities: Set<string>;
    private verbose: boolean;
    private maxContiguous: number;
    private regex: RegExp;
    private numHashes: number;
    private totalExpectedHashes: number;
    private impossibleRegex: RegExp;


    constructor (graphic: string[], numericals:number[], verbose: boolean = false) {
        this.verbose = verbose
        this.statuses = graphic;
        this.numericalRecords = numericals;
        this.maxContiguous = Math.max(...this.numericalRecords);
        this.previousPossibilities = new Set<string>();
        let regexString = this.numericalRecords.map((x)=>`#{${x}}`).join("\\.+");
        this.regex = new RegExp(regexString);
        this.numHashes = this.statuses.filter((x)=>x=="#").length;
        this.totalExpectedHashes = this.numericalRecords.reduce(sumFunc);

        let impossibleRegexString = "^[\\.?]*"
        impossibleRegexString += this.numericalRecords.map((x)=>`[#?]{${x}}`).join("[?\\.]+");
        impossibleRegexString += "[\\.?]*$"
        this.impossibleRegex = new RegExp(impossibleRegexString);
    }


    static fromString (line: string, verbose = false): SpringRecord {
        var [graphic, numbers] = line.split(" ");

        graphic = `${graphic}?`.repeat(5).slice(0,-1);
        numbers = `${numbers},`.repeat(5).slice(0,-1);

        return new SpringRecord(
            graphic.split(""),
            numbers.split(",").map((x)=>parseInt(x)),
            verbose);
    }

}


export {SpringRecord};

import * as fs from 'fs';
import * as readline from 'readline';


if (require.main === module) {

    // Create a readable stream for the file
    const fileStream: fs.ReadStream = fs.createReadStream(file);

    // Create an interface for reading the file line by line
    const rl: readline.Interface = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity // Recognize all instances of CR LF ('\r\n') as a single line break.
    });


    var records: SpringRecord[] = [];


    // On Line
    rl.on('line', (line: string) => {

        
        records.push(SpringRecord.fromString(line));
    });

    // On End
    rl.on('close', () => {

        let i = 0;
        let possibilities = records
            .map((x) => {
                console.log(`Record ${i++}`);
                return possibilities2(x.statuses.join(""), x.numericalRecords, new Map<string, number>());
            })

            // possibilities.forEach((x)=>console.log(x));
            console.log(possibilities.reduce(sumFunc));
    
    });

    // On error
    fileStream.on('error', (error: NodeJS.ErrnoException) => {
    console.error(`Error reading the file: ${error.message}`);
    });

}