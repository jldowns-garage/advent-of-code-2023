/* spellchecker: disable */
import {SpringRecord} from "./day12"

test('Possibilities are calculated correctly.', () => {

    let t = 0;

    console.log("Testing...");
    let record = SpringRecord.fromString("???.### 1,1,3");
    expect(record.possibilities()).toEqual(1);

    console.log("Testing...");
    record = SpringRecord.fromString(".??..??...?##. 1,1,3");
    expect(record.possibilities()).toEqual(16384);

    console.log("Testing...");
    record = SpringRecord.fromString("?#?#?#?#?#?#?#? 1,3,1,6");
    expect(record.possibilities()).toEqual(1);

    console.log("Testing...");
    record = SpringRecord.fromString("????.#...#... 4,1,1");
    expect(record.possibilities()).toEqual(16);

    console.log("Testing...");
    record = SpringRecord.fromString("????.######..#####. 1,6,5");
    expect(record.possibilities()).toEqual(2500);

    console.log("Testing...");
    record = SpringRecord.fromString("?###???????? 3,2,1");
    expect(record.possibilities()).toEqual(506250);
});
