

// var cache = new Map<string, number>();


function possibilities2(graphic: string, nums: number[], cache: Map<string, number>): number {

    // console.log(graphic);
    // let fingerprint = something + graphic.indexOf(?) + 

    let rs = "^[\\.?]*";
    let solved = 0;
    let index = 0;
    for (let i=0; i<nums.length; i++) {
        rs = rs + `[#]{${nums[i]}}[\\.$]+`
        let pr = new RegExp(rs);
        let m = graphic.match(pr);
        if (m) {
            index = m["index"]! + m[0].length;
            solved++;
        };
    }
    let fingerprint = `${graphic.slice(index, graphic.length)}${nums.slice(solved,nums.length).join("")}`;
    
    if (cache.has(fingerprint)) {
        return cache.get(fingerprint)!;
    }

    let regexString = nums.map((x)=>`[#?]{${x}}`).join("[\\.?]+");
    regexString = "^[\\.?]*" + regexString + "[\\.?]*$";
    let possibleRegex = new RegExp(regexString);
    
    if (!graphic.match(possibleRegex)) {
        // console.log("can't match")
        cache.set(fingerprint, 0);
        return 0; }

    if (!graphic.includes("?")) {
        cache.set(fingerprint, 1);
        return 1;
    }

    let r = possibilities2(graphic.replace("?","."), nums, cache) + possibilities2(graphic.replace("?","#"), nums, cache);
    cache.set(fingerprint, r);
    return r;
}

export {possibilities2};
