
class Round {
  red = 0;
  green = 0;
  blue = 0;

  toString(): string {
    return `red:${this.red}, green:${this.green}, blue:${this.blue}`
  }

  isValid(): boolean {
    
    return (
      this.red <= 12 &&
      this.blue <= 14 &&
      this.green <= 13
    );
 }

  static fromString(s: string): Round {
    // Example string:
    // "14 blue, 1 red, 3 green"

    var newRound = new Round();

    for (var c of s.split(",")) {
      var [num, color] = c.trim().split(" ");
      if (color == "blue") { newRound.blue = parseInt(num); }
      if (color == "green") { newRound.green = parseInt(num); }
      if (color == "red") { newRound.red = parseInt(num); }
    }

    return newRound;
  }
}
1


class Game {
  rounds: Round[] = [];
  game_number = 0;

  minInventory(): Round {
    let r = new Round()

    r.red = Math.max(...this.rounds.map((x)=>x.red));
    r.blue = Math.max(...this.rounds.map((x)=>x.blue));
    r.green = Math.max(...this.rounds.map((x)=>x.green));

    return r;
  }

  power(): number {
    let r = this.minInventory();
    return r.red * r.blue * r.green;
  }

  toString(): string {
    let s = `Game ${this.game_number}: `;
    for (var r of this.rounds) { s += `; ${r.toString()}`; }
    s += `valid? ${this.isValid()}`
    s += ` power: ${this.power()}`
    s += ` min: ${this.minInventory()}`

    return s;
  }

  isValid(): boolean {
    return this.rounds.every((x)=> x.isValid());
  }


  static fromLine(line: string): Game {
    var newGame = new Game();
    var [gameText, roundsText] = line.split(":");
    newGame.rounds = roundsText.split(";").map((x)=>Round.fromString(x));
    // Extract Game number
    const matches = gameText.match(/\d+/);
    newGame.game_number = matches ? parseInt(matches[0], 10) : 0;
    
    // 0 is an invalid number.
    if (newGame.game_number == 0){ throw new Error(`couldn't parse number for $(line)`); }
    return newGame
  }
}

/////////////////
// Reading File
/////////////////

import * as fs from 'fs';
import * as readline from 'readline';

const file: string = 'day02/input.txt';

var games: Game[] = [];

// Create a readable stream for the file
const fileStream: fs.ReadStream = fs.createReadStream(file);

// Create an interface for reading the file line by line
const rl: readline.Interface = readline.createInterface({
  input: fileStream,
  crlfDelay: Infinity // Recognize all instances of CR LF ('\r\n') as a single line break.
});

// Event listener for each line in the file
rl.on('line', (line: string) => {
  let g = Game.fromLine(line);
  games.push(g);
  console.log(g.toString());
});

// Event listener for the completion of the file reading
rl.on('close', () => {
  console.log('Finished reading the file.');

  let sum: number = games
    .map((x)=>x.power())
    .reduce((a,b)=>a+b);

  console.log(sum);

});

// Event listener for any errors during reading
fileStream.on('error', (error: NodeJS.ErrnoException) => {
  console.error(`Error reading the file: ${error.message}`);
});
