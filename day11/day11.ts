// Day 011
const file: string = 'day11/input.txt';


class Galaxy {
    
    constructor(
        public row: number,
        public col: number,
    ) {}

    distanceTo(g: Galaxy) {
        return Math.abs(this.row-g.row) + Math.abs(this.col-g.col)
    }
}

class Universe {
    galaxies: Galaxy[] = [];
    private pMaxRow: number = -1;
    private pMaxCol: number = -1;

    addGalaxy(g: Galaxy) {
        this.galaxies.push(g);
    }

    maxRow() {
        // if (this.pMaxRow == -1) {
        if (true) {
            this.pMaxRow = Math.max(...this.galaxies.map((x)=>x.row));
        }
        return this.pMaxRow;
    }

    maxCol() {
        // if (this.pMaxCol == -1) {
        if (true) {
            this.pMaxCol = Math.max(...this.galaxies.map((x)=>x.col));
        }
        return this.pMaxCol;
    }

    resetMax() {
        this.pMaxCol = -1;
        this.pMaxRow = -1;
    }

    galaxyPairs(): Galaxy[][] {

        let pairs: Galaxy[][] = [];

        for (let i=0; i<this.galaxies.length - 1; i++) {
            for (let j=i+1; j<this.galaxies.length; j++) {
                pairs.push([this.galaxies[i], this.galaxies[j]]);
            }

        }
        return pairs;
        
    }

    expandGalaxy() {

        let factor = 1000000-1;

        // Find empty rows
        let emptyRows:number[] = []

        for(let r =0; r<=this.maxRow(); r++) {
            if (this.galaxies.filter((x)=>x.row==r).length == 0) {
                emptyRows.push(r);
            }
        }

        while (emptyRows.length>0) {
            let row = emptyRows.shift()!;
            this.galaxies.filter((x)=>x.row>row).forEach((x)=>x.row+=factor);
            emptyRows = emptyRows.map((x)=>x+factor);
        }

        // Empty Columns
        let emptyCols: number[] = [];

        for(let c=0; c<=this.maxCol(); c++) {
            if (this.galaxies.filter((x)=>x.col==c).length == 0) {
                emptyCols.push(c);
            }
        }

        while (emptyCols.length>0) {
            let col = emptyCols.shift()!;
            this.galaxies.filter((x)=>x.col>col).forEach((x)=>x.col+=factor);
            emptyCols = emptyCols.map((x)=>x+factor);
        }
        
        this.resetMax();
    }

    print() {
        console.log(`${this.maxRow()}, ${this.maxCol()}`);
        for (let r = 0; r<=this.maxRow(); r++) {
            let s = "";
            for (let c = 0; c<=this.maxCol(); c++) {
                if (this.galaxies.filter((x)=>(x.row==r&&x.col==c)).length!=0) {
                    s += "#";
                } else {
                    s+=".";
                }
            }
            console.log(s);
        }

        console.log(this.galaxies);
    }

}

export {};

import * as fs from 'fs';
import * as readline from 'readline';

if (require.main === module) {


    // Create a readable stream for the file
    const fileStream: fs.ReadStream = fs.createReadStream(file);

    // Create an interface for reading the file line by line
    const rl: readline.Interface = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity // Recognize all instances of CR LF ('\r\n') as a single line break.
    });


    var r = -1;
    var universe = new Universe();

    // On Line
    rl.on('line', (line: string) => {

        r += 1;
        for (var c=0; c<line.length; c++) {
            if (line[c] == "#") {
                universe.addGalaxy(new Galaxy(r, c));
            }
        }

    });

    // On End
    rl.on('close', () => {

        universe.expandGalaxy();

        let distances = universe.galaxyPairs()
            .map((x)=> x[0].distanceTo(x[1]))
            .reduce((a,b)=>a+b);

        console.log(distances);

    });

    // On error
    fileStream.on('error', (error: NodeJS.ErrnoException) => {
    console.error(`Error reading the file: ${error.message}`);
    });

}