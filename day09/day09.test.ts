/* spellchecker: disable */

import {nextValue, previousValue} from "./day09"

test('Next Value works.', () => {

    let values = [0, 3, 6, 9, 12, 15];
    let expectedNext = 18;
    let answer = nextValue(values);

    values = [1, 3, 6, 10, 15, 21];
    expectedNext = 28;
    answer = nextValue(values);

    values = [10, 13, 16, 21, 30, 45];
    expectedNext = 68;
    answer = nextValue(values);

    expect(answer).toEqual(expectedNext);
});

test('Previous Value works.', () => {

    let values = [0, 3, 6, 9, 12, 15];
    let expectedPrevious = -3;
    let answer = previousValue(values);

    values = [1, 3, 6, 10, 15, 21];
    expectedPrevious = 0;
    answer = previousValue(values);

    values = [10, 13, 16, 21, 30, 45];
    expectedPrevious = 5;
    answer = previousValue(values);

    expect(answer).toEqual(expectedPrevious);
});