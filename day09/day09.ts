// Day 09
const file: string = 'day09/input.txt';


function nextValue(values: number[]): number {

    let subValues: number[] = [];

    for (let i=0; i<values.length - 1; i++) {
        subValues.push(values[i+1]-values[i]);
    }

    if (values.every((x)=>x==0)) {
        return 0;
    } else {
        return values.slice(-1)[0] + nextValue(subValues);
    }
}

function previousValue(values: number[]): number {

    let subValues: number[] = [];

    for (let i=0; i<values.length - 1; i++) {
        subValues.push(values[i+1]-values[i]);
    }

    if (values.every((x)=>x==0)) {
        return 0;
    } else {
        return values[0] - previousValue(subValues);
    }
}

export {nextValue, previousValue};

import * as fs from 'fs';
import * as readline from 'readline';

if (require.main === module) {

    var total = 0;

    // Create a readable stream for the file
    const fileStream: fs.ReadStream = fs.createReadStream(file);

    // Create an interface for reading the file line by line
    const rl: readline.Interface = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity // Recognize all instances of CR LF ('\r\n') as a single line break.
    });

    // On Line
    rl.on('line', (line: string) => {

        let values = line.split(/\s+/).map((x)=>parseInt(x.trim()));
        if (values.some((x)=>isNaN(x))) {
            console.log(`NaN!    ${line}`);
            console.log(`Parsed: ${values}`);

        }
        total += previousValue(values);

    });

    // On End
    rl.on('close', () => {

        console.log(total);
    
    });

    // On error
    fileStream.on('error', (error: NodeJS.ErrnoException) => {
    console.error(`Error reading the file: ${error.message}`);
    });

}