enum Category {
  Number,
  Symbol
}

class Entity {
    // This covers both numbers and symbols
    category: Category;
    start: number;
    end: number;
    value: string;

    constructor(
      category: Category,
      start: number,
      end: number,
      value: string) {
        this.category = category;
        this.start = start;
        this.end = end;
        this.value = value;
    }

    adjacentTo(entity: Entity): boolean {
      return (entity.start <= (this.end+1) && entity.end >= (this.start-1))
    }

    asNumber(): number {
      if (this.category == Category.Symbol) {throw new Error(`${this.value} is not a number.`)}
      return parseInt(this.value);
    }
}


function entitiesFromLine(line: string): Entity[] {
  let regex = /(\d+)|[^\d\.]/gd;
  type RegExpMatchArrayWithIndices = RegExpMatchArray & { indices: Array<[number, number]> };
  let a: RegExpExecArray|null;
  let entities: Entity[] = [];

  while ((a = regex.exec(line)) !== null) {
    entities.push(new Entity(
      Number.isNaN(parseInt(a[0])) ? Category.Symbol : Category.Number,
      (a as RegExpMatchArrayWithIndices).indices[0][0],
      (a as RegExpMatchArrayWithIndices).indices[0][1] - 1,
      a[0]
      ))
  }

  return entities;
}

class LineWindow {
  // Keeps track of the lines we're working with. (3 total)
  lines: string[] = []

  constructor() {
    this.lines.push(".........");
  }

  addLine(line: string) {
    // adds a line. Drops one if the lines go more than 3.
    this.lines.push(line)
    if (this.lines.length > 3) { this.lines.shift() }
  }

  length(): number {return this.lines.length}

  isFull(): boolean {return this.length() >= 3}

  asString(): string { return this.lines.join("\n"); }

  numbersInWindow(): Entity[] {
    // returns a list of numbers in the center of the window.
    return entitiesFromLine(this.lines[1])
      .filter((x)=> x.category == Category.Number);
  }

  gearsInWindow(): Entity[] {
    // returns a list of '*' in the center of the window.
    return entitiesFromLine(this.lines[1])
      .filter((x)=> x.value == "*");
  }

  symbolsInWindow(): Entity[] {
    // returns the symbol entities from all lines in the window
    return this.lines
      .map((x)=>entitiesFromLine(x))
      .flat()
      .filter((x)=>x.category == Category.Symbol);
  }

  allNumbersInWindow(): Entity[] {
    // returns the symbol entities from all lines in the window
    return this.lines
      .map((x)=>entitiesFromLine(x))
      .flat()
      .filter((x)=>x.category == Category.Number);
  }
}

/////////////////
// Reading File
/////////////////

import * as fs from 'fs';
import * as readline from 'readline';

const file: string = 'day03/input.txt';
var lineWindow = new LineWindow();
var goodNumbers: number[] = []


// Create a readable stream for the file
const fileStream: fs.ReadStream = fs.createReadStream(file);

// Create an interface for reading the file line by line
const rl: readline.Interface = readline.createInterface({
  input: fileStream,
  crlfDelay: Infinity // Recognize all instances of CR LF ('\r\n') as a single line break.
});

// On Line
rl.on('line', (line: string) => {
  lineWindow.addLine(line);
  
  if (lineWindow.isFull()) {
    let numbers = lineWindow.allNumbersInWindow();
    let symbols = lineWindow.symbolsInWindow();
    let gears = lineWindow.gearsInWindow();

    for (var g of gears){

      let adjacentNumbers = numbers.filter((num)=>g.adjacentTo(num))
      if (adjacentNumbers.length == 2) {
        goodNumbers.push(adjacentNumbers
          .map((x)=> x.asNumber())
          .reduce((a,b)=>a*b))}
      }
    }
  });

// On End
rl.on('close', () => {
  console.log('Finished reading the file.');
  console.log(goodNumbers);
  console.log(goodNumbers.reduce((a,b)=>{return a+b; }));
});

// On error
fileStream.on('error', (error: NodeJS.ErrnoException) => {
  console.error(`Error reading the file: ${error.message}`);
});

export {Category, Entity, entitiesFromLine}