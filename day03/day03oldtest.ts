import {Entity, Category, entitiesFromLine} from "./day03"

test('adjacent entities are detected', () => {

  let a = new Entity(Category.Number, 2, 4, "111")
  let b = new Entity(Category.Number, 4, 4, "@")
  let t = a.adjacentTo(b);
  expect(t).toBeTruthy;

  a = new Entity(Category.Number, 2, 4, "111")
  b = new Entity(Category.Number, 0, 10, "@")
  t = a.adjacentTo(b);
  expect(t).toBeTruthy;

  a = new Entity(Category.Number, 2, 4, "111")
  b = new Entity(Category.Number, 3, 3, "@")
  t = a.adjacentTo(b);
  expect(t).toBeTruthy;

  a = new Entity(Category.Number, 2, 4, "111")
  b = new Entity(Category.Number, 5, 5, "@")
  t = a.adjacentTo(b);
  expect(t).toBeTruthy;

  a = new Entity(Category.Number, 2, 4, "111")
  b = new Entity(Category.Number, 5, 8, "@")
  t = a.adjacentTo(b);
  expect(t).toBeTruthy;
});

test('non-adjacent entities are detected', () => {

  let a = new Entity(Category.Number, 2, 4, "111")
  let b = new Entity(Category.Number, 6, 10, "@")
  let t = a.adjacentTo(b);
  expect(t).toBeFalsy;
});


test('Entities are build correctly from lines', () => {

  let entities: Entity[];
  let expectedEntities: Entity[];
  let line: string

  line = "467..114.."
  entities = entitiesFromLine(line);
  expectedEntities = [
    new Entity(Category.Number, 0, 2, "467"),
    new Entity(Category.Number, 5, 7, "114"),
  ];

  expect(entities).toEqual(expectedEntities);

  line = "467#..!"
  entities = entitiesFromLine(line);
  expectedEntities = [
    new Entity(Category.Number, 0, 2, "467"),
    new Entity(Category.Symbol, 3, 3, "#"),
    new Entity(Category.Symbol, 6, 6, "!"),
  ];

  expect(entities).toEqual(expectedEntities);
});