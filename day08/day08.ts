class LocationNode {
    name: string = "";
    left: string = "";
    right: string = "";

    constructor(name: string, left: string, right: string) {
        this.name = name;
        this.left = left;
        this.right = right;
    }

    nextFromString(s: string): string {
        if (s == "R") {
            return this.right;
        } else if (s == "L") {
            return this.left;
        } else {
            throw new Error(`${s} is not a recognized direction.`);
        }
    }

    static fromLine(line: string): LocationNode {
        let newLN = new LocationNode("", "", "");
        let matches = line.match(/[\d|\w]+/g)
        if (matches) { [newLN.name, newLN.left, newLN.right] = matches; }
        return newLN;
    }
}

class RepeatString {
    originalString: string;
    index = -1;

    constructor(s: string) { 
        this.originalString = s;
    }

    next(): string {
        this.index += 1;
        if (this.index >= this.originalString.length) {
            this.index = 0;
        }
        return this.originalString[this.index];
    }

    reset() {
        this.index = -1;
    }

}


// From: https://stackoverflow.com/questions/47047682/least-common-multiple-of-an-array-values-using-euclidean-algorithm
var gcd = function (a: number, b: number): number {
    return a ? gcd(b % a, a) : b;
}

var lcm = function (a: number, b: number): number {
    return a * b / gcd(a, b);
}

export {LocationNode, RepeatString};

import * as fs from 'fs';
import * as readline from 'readline';

if (require.main === module) {

    const file: string = 'day08/input.txt';

    var locations = new Map<string, LocationNode>();
    var directions: RepeatString;

    // Create a readable stream for the file
    const fileStream: fs.ReadStream = fs.createReadStream(file);

    // Create an interface for reading the file line by line
    const rl: readline.Interface = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity // Recognize all instances of CR LF ('\r\n') as a single line break.
    });

    // On Line
    rl.on('line', (line: string) => {

        if (line.length < 1) { 
            // skip
        } else if (line.includes("=")) {
            let l = LocationNode.fromLine(line);
            // console.log(line);
            // console.log(l);
            locations.set(l.name, l);
        } else {
        // This is probably the R/L line.
            directions = new RepeatString(line);
        }
    });

    // On End
    rl.on('close', () => {

        var ghostLocations = Array.from(locations.keys()).filter((x: string)=> x.slice(-1) == "A")
        var rotations: number[] = [];
        console.log(`Starting locations: ${ghostLocations}`);

        for (var location of ghostLocations) {
            console.log("");

            var i = 0;
            directions.reset();
            // let location: string = ghostLocations[glIndex]; // start
            var solvedLocations = new Map<string, number>(); // <[location, directionIndex], turn>
            var exits: string[] = [];

            while(true) {
                i++;
                let d = directions.next();
                let di = directions.index;

                if (location.slice(-1) == "Z") { exits.push(`${location}${di}`); }

                if (solvedLocations.has(`${location}${di}`)) {
                    let rotationPoint = solvedLocations.get(`${location}${di}`)!;
                    console.log(`Found rotation.`)
                    console.log(`Rotation point: ${rotationPoint}`)
                    console.log(`Found after ${i} movements.`)
                    console.log(`${exits.length} exits exist: ${exits}`)
                    let rot = (i-rotationPoint)/281;
                    console.log(`${rot} rotations.`)
                    rotations.push(rot)
                    break;
                } else {
                    solvedLocations.set(`${location}${di}`, i);
                    location = locations.get(location)?.nextFromString(d)!
                }
            }
        }

        let rotLCM = rotations.reduce(lcm);
        console.log(`LCM of all rotations: ${rotLCM}`);
        console.log(`Answer: ${rotLCM * 281}`)

    });

    // On error
    fileStream.on('error', (error: NodeJS.ErrnoException) => {
    console.error(`Error reading the file: ${error.message}`);
    });

}