class CardPile {

    cardCount: number[] = []

    addCard(index: number) {
        while (this.cardCount.length <= index) { this.cardCount.push(1); }
        this.cardCount[index] += 1;
    }

    getCardCount(index: number): number {
        while (this.cardCount.length <= index) { this.cardCount.push(1); }
        return this.cardCount[index];
    }
}



function myWinners(winners: number[], mine: number[]): number[] {
    return winners
        .filter((x)=>mine.includes(x));
}

/////////////////
// Reading File
/////////////////

import * as fs from 'fs';
import * as readline from 'readline';

const file: string = 'day04/input.txt';
var points = 0;
var cards = 0
var cardPile = new CardPile();

// Create a readable stream for the file
const fileStream: fs.ReadStream = fs.createReadStream(file);

// Create an interface for reading the file line by line
const rl: readline.Interface = readline.createInterface({
  input: fileStream,
  crlfDelay: Infinity // Recognize all instances of CR LF ('\r\n') as a single line break.
});

// On Line
rl.on('line', (line: string) => {

    let [left, right] = line.split("|");
    let winners = left.split(":")[1].trim().split(/\s+/).map((x)=>parseInt(x));
    let cardNumberString = left.match(/\d+/);
    let cardNumber = cardNumberString ? parseInt(cardNumberString[0]): 0;
    let mine = right.trim().split(/\s+/).map((x)=>parseInt(x));
    let wins = myWinners(winners, mine);
    let currentPoints = 0;

    console.log(line)
    console.log(`(${cardPile.getCardCount(cardNumber)} cards.)`)

    for (var j = 0; j < cardPile.getCardCount(cardNumber); j++) {
        for (var i = 0; i<wins.length; i++) {
            cardPile.addCard( cardNumber + i + 1)
        }
    }

    cards += cardPile.getCardCount(cardNumber);
  });

// On End
rl.on('close', () => {
    console.log(cards);
});

// On error
fileStream.on('error', (error: NodeJS.ErrnoException) => {
  console.error(`Error reading the file: ${error.message}`);
});
