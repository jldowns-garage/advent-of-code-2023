const file: string = 'day08/input.txt';
import * as fs from 'fs';
import * as readline from 'readline';


export {};

if (require.main === module) {


    // Create a readable stream for the file
    const fileStream: fs.ReadStream = fs.createReadStream(file);

    // Create an interface for reading the file line by line
    const rl: readline.Interface = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity // Recognize all instances of CR LF ('\r\n') as a single line break.
    });

    // On Line
    rl.on('line', (line: string) => {

    });

    // On End
    rl.on('close', () => {
    
    });

    // On error
    fileStream.on('error', (error: NodeJS.ErrnoException) => {
    console.error(`Error reading the file: ${error.message}`);
    });

}