import * as fs from 'fs';
import * as readline from 'readline';

var calibrations = 0

const file: string = 'day01/input.txt';

function firstNumber(input: string): number {
  let parsed: string = ""

  for (var s of input) {
    parsed += s
    if (parsed.search(/1|one/) > -1) { return 1 }
    if (parsed.search(/2|two/) > -1) { return 2 }
    if (parsed.search(/3|three/) > -1) { return 3 }
    if (parsed.search(/4|four/) > -1) { return 4 }
    if (parsed.search(/5|five/) > -1) { return 5 }
    if (parsed.search(/6|six/) > -1) { return 6 }
    if (parsed.search(/7|seven/) > -1) { return 7 }
    if (parsed.search(/8|eight/) > -1) { return 8 }
    if (parsed.search(/9|nine/) > -1) { return 9 }
  }
  return 0
}

function lastNumber(input: string): number {
  let parsed: string = ""

  for (var i = input.length; i--; ) {
    parsed = input[i] + parsed
    if (parsed.search(/1|one/) > -1) { return 1 }
    if (parsed.search(/2|two/) > -1) { return 2 }
    if (parsed.search(/3|three/) > -1) { return 3 }
    if (parsed.search(/4|four/) > -1) { return 4 }
    if (parsed.search(/5|five/) > -1) { return 5 }
    if (parsed.search(/6|six/) > -1) { return 6 }
    if (parsed.search(/7|seven/) > -1) { return 7 }
    if (parsed.search(/8|eight/) > -1) { return 8 }
    if (parsed.search(/9|nine/) > -1) { return 9 }
  }
  return 0
}

function extractCalibration(s: string): number {

  return firstNumber(s) * 10 + lastNumber(s)

}

/////////////////
// Reading File
/////////////////

// Create a readable stream for the file
const fileStream: fs.ReadStream = fs.createReadStream(file);

// Create an interface for reading the file line by line
const rl: readline.Interface = readline.createInterface({
  input: fileStream,
  crlfDelay: Infinity // Recognize all instances of CR LF ('\r\n') as a single line break.
});

// Event listener for each line in the file
rl.on('line', (line: string) => {
  let calibrationNumber = extractCalibration(line)
  calibrations += calibrationNumber
  console.log(`Line from file: ${line}, ${calibrationNumber}`);
  console.log(calibrations)
});

// Event listener for the completion of the file reading
rl.on('close', () => {
  console.log('Finished reading the file.');
});

// Event listener for any errors during reading
fileStream.on('error', (error: NodeJS.ErrnoException) => {
  console.error(`Error reading the file: ${error.message}`);
});
