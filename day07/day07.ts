/* spellchecker: disable */
// Day 7!

// Examples:
// 32T3K 765
// T55J5 684
// KK677 28
// KTJJT 220
// QQQJA 483

enum HandType {
    FiveOfAKind,
    FourOfAKind,
    FullHouse,
    ThreeOfAKind,
    TwoPair,
    OnePair,
    HighCard
}

const handTypeValues = new Map<HandType, number>([
    [HandType.FiveOfAKind , 7],
    [HandType.FourOfAKind , 6],
    [HandType.FullHouse   , 5],
    [HandType.ThreeOfAKind, 4],
    [HandType.TwoPair , 3],
    [HandType.OnePair , 2],
    [HandType.HighCard, 1 ]
]);

const cardValues = new Map<string, number>([
    ["2", 1,],
    ["3", 2,],
    ["4", 3,],
    ["5", 4,],
    ["6", 5,],
    ["7", 6,],
    ["8", 7,],
    ["9", 8,],
    ["T", 9,],
    ["J", 0,],
    ["Q", 11,],
    ["K", 12,],
    ["A", 13,],
]);

class Hand {
    cards: string;

    constructor(cards: string) {
        this.cards = cards;
    }

    handType(): HandType {

        // One special case: all J's.
        if (this.cards == "JJJJJ") { return HandType.FiveOfAKind; }

        var cardCountTracker: { [key: string]: number } = {}

        let js = 0;
        // Find matching cards
        for (var c of this.cards) {
            if (c == "J") {
                js += 1;
                continue;
            }
            if (cardCountTracker[c]) {
                cardCountTracker[c] += 1;

            } else {
                cardCountTracker[c] = 1;
            }
        }
        let counts = Object.values(cardCountTracker);

        let maxCountIndex = counts.indexOf(Math.max(...counts));
        counts[maxCountIndex] += js;

        if (counts.includes(5)) { return HandType.FiveOfAKind; }
        if (counts.includes(4)) { return HandType.FourOfAKind; }
        if (counts.includes(3) && counts.includes(2)) { return HandType.FullHouse; }
        if (counts.includes(3)) { return HandType.ThreeOfAKind; }
        if (counts.filter((x)=>x==2).length == 2) { return HandType.TwoPair; }
        if (counts.includes(2)) { return HandType.OnePair; }
        return HandType.HighCard;
    }

    highCard(): string {
        return this.cards.split("").sort((a,b)=>cardValues.get(b)! - cardValues.get(a)! )[0]
    }

    isBiggerThan(h: Hand): boolean {

        let thisHandTypeValue = handTypeValues.get(this.handType())!;
        let thatHandTypeValue = handTypeValues.get(h.handType())!;
        if (thisHandTypeValue > thatHandTypeValue) { return true; }
        if (thisHandTypeValue < thatHandTypeValue) { return false; }

        // Same type, now check high cards
        for (let i=0; i<this.cards.length; i++) {
            let thisCardValue = cardValues.get(this.cards[i])!;
            let thatCardValue = cardValues.get(h.cards[i])!;
            if (thisCardValue > thatCardValue) { return true; }
            if (thisCardValue < thatCardValue) { return false; }
            // I guess they're equal! On to the next card.
        }
        
        // These must be the same decks...
        if (this.cards != h.cards) { throw new Error(`${this.cards} and ${h.cards} are evaluating as equal.`)}

        return false; // Equal cards are not bigger.

    }
}

/////////////////
// Reading File
/////////////////

import * as fs from 'fs';
import * as readline from 'readline';
const file: string = 'day07/input.txt';

if (require.main === module) {

var handsAndBets: [Hand, number][] = []

// Create a readable stream for the file
const fileStream: fs.ReadStream = fs.createReadStream(file);

// Create an interface for reading the file line by line
const rl: readline.Interface = readline.createInterface({
  input: fileStream,
  crlfDelay: Infinity // Recognize all instances of CR LF ('\r\n') as a single line break.
});

// On Line
rl.on('line', (line: string) => {
    let hand_bid = line.split(" ");
    let h = new Hand(hand_bid[0]);
    let b = parseInt(hand_bid[1]);
    handsAndBets.push([h, b]);
});

// On End
rl.on('close', () => {

    handsAndBets.sort((a,b)=>{
        if (a[0].isBiggerThan(b[0])) { return 1; }
        return -1;
    })

    console.log(handsAndBets);

    var total = 0;
    for (let i=0; i<handsAndBets.length; i++) {
        total += (i+1)*handsAndBets[i][1];
    }

    console.log(total);

});

// On error
fileStream.on('error', (error: NodeJS.ErrnoException) => {
  console.error(`Error reading the file: ${error.message}`);
});


}
export {Hand, HandType}