/* spellchecker: disable */

import {Hand, HandType} from "./day07"

test('Hand is made from input.', () => {

  let h = new Hand("12Q45");
  expect(h).toBeTruthy();
});

test("Hand types are calculated correctly.", () => {
    let h = new Hand("AAAAA");
    expect(h.handType()).toEqual(HandType.FiveOfAKind);

    h = new Hand("AAAA1");
    expect(h.handType()).toEqual(HandType.FourOfAKind);

    h = new Hand("AAA11");
    expect(h.handType()).toEqual(HandType.FullHouse);

    h = new Hand("A9AA2");
    expect(h.handType()).toEqual(HandType.ThreeOfAKind);

    h = new Hand("1A3A3");
    expect(h.handType()).toEqual(HandType.TwoPair);

    h = new Hand("A3A12");
    expect(h.handType()).toEqual(HandType.OnePair);

    h = new Hand("A9312");
    expect(h.handType()).toEqual(HandType.HighCard);
});

test("New Joker rule done correctly.", () => {
  let h = new Hand("AAAAJ");
  expect(h.handType()).toEqual(HandType.FiveOfAKind);

  h = new Hand("AJAA1");
  expect(h.handType()).toEqual(HandType.FourOfAKind);

  h = new Hand("AAJ11");
  expect(h.handType()).toEqual(HandType.FullHouse);

  h = new Hand("A9JA2");
  expect(h.handType()).toEqual(HandType.ThreeOfAKind);

  h = new Hand("A3J12");
  expect(h.handType()).toEqual(HandType.OnePair);

  h = new Hand("A9312");
  expect(h.handType()).toEqual(HandType.HighCard);
});

test('Hands are compared correctly.', () => {

    let h1 = new Hand("AA126");
    let h2 = new Hand("AA134");
    expect(h1.isBiggerThan(h2)).toBeFalsy();
    expect(h2.isBiggerThan(h1)).toBeTruthy();

    h1 = new Hand("AA123");
    h2 = new Hand("AA112");
    expect(h1.isBiggerThan(h2)).toBeFalsy();
    expect(h2.isBiggerThan(h1)).toBeTruthy();

    h1 = new Hand("KJ123");
    h2 = new Hand("K3K12");
    expect(h1.isBiggerThan(h2)).toBeFalsy();
    expect(h2.isBiggerThan(h1)).toBeTruthy();

    h1 = new Hand("KJ123");
    h2 = new Hand("JJJJJ");
    expect(h1.isBiggerThan(h2)).toBeFalsy();
    expect(h2.isBiggerThan(h1)).toBeTruthy();

  });