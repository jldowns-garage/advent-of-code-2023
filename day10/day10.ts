// Day 09
const file: string = 'day10/input.txt';

enum Orientation {
    CW = 1,
    CCW = -1,
    Straight = 0
}

class Maze {
    pipes: Pipe[] = [];
    epoch: number = 0; // how many times we've reset the maze
    orientation: Orientation = Orientation.Straight;
    maxRows: number = -1;
    maxCols: number = -1;

    // These are for moving around the maze
    currentRow: number = -1;
    currentCol: number = -1;
    cameFrom: string = "."; // the last direction we came from (opposite that we move)
    steps: number = 0;
    orientationCount: Map<Orientation, number>;

    constructor() {
        this.orientationCount = new Map<Orientation, number>([
            [Orientation.CCW, 0],
            [Orientation.CW, 0],
            [Orientation.Straight, 0],
        ]);
    }

    maxRow(): number {
        if (this.maxRows == -1) { this.maxRows = this.pipes.reduce((max,comp)=>comp.row>max?comp.row:max, 0) + 1; }
        return this.maxRows;
        }

    maxCol(): number {
        if (this.maxCols == -1) { this.maxCols = this.pipes.reduce((max,comp)=>comp.col>max?comp.col:max, 0) + 1; }
        return this.maxCols;
        }

    pipeAt(row: number, col: number):Pipe {
        let r = this.pipes.filter((x)=>x.row==row&&x.col==col);
        if (r.length == 0) { throw new Error(`No pipes found at row ${row}, col ${col}.`)};
        if (r.length > 1) { throw new Error(`Multiple pipes found at row ${row}, col ${col}: ${r}`)};
        return r[0];
    }

    startingPipe() {
        let r = this.pipes.filter((x)=>x.value=="S");
        if (r.length == 0) { throw new Error(`Starting pipes found.`)};
        if (r.length > 1) { throw new Error(`Multiple starting pipes found.`)};
        return r[0];
    }

    resetMoves() {
        this.currentRow = -1;
        this.currentCol = -1;
        this.cameFrom = "."; // the last direction we came from (opposite that we move)
        // this.epoch += 1;
        this.steps = 0;
        let sp = this.startingPipe();
        sp.minSteps = 0;
        this.currentCol = sp.col;
        this.currentRow = sp.row;
        this.cameFrom = sp.exits[(this.epoch-1)%2]; // choose the path not previously traveled.
    }

    nextMove() {
        // move and update
        let p = this.pipeAt(this.currentRow, this.currentCol);
        p.isMain = true; // this is part of the main pipe
        let nextDirection = p.exits.filter((x)=>x!=this.cameFrom)[0];

        if (nextDirection == "N") {
            if (this.cameFrom == "E") {
                let o = this.orientationCount.get(Orientation.CW)!
                this.orientationCount.set(Orientation.CW, o + 1);
            } else if (this.cameFrom == "W") {
                let o = this.orientationCount.get(Orientation.CCW)!
                this.orientationCount.set(Orientation.CCW, o + 1);
            }
            this.currentRow -= 1;
            this.cameFrom = "S";
            
        } else if (nextDirection == "S") {
            if (this.cameFrom == "E") {
                let o = this.orientationCount.get(Orientation.CCW)!
                this.orientationCount.set(Orientation.CCW, o + 1);
            } else if (this.cameFrom == "W") {
                let o = this.orientationCount.get(Orientation.CW)!
                this.orientationCount.set(Orientation.CW, o + 1);
            }
            this.currentRow += 1;
            this.cameFrom = "N";
        } else if (nextDirection == "E") {
            if (this.cameFrom == "N") {
                let o = this.orientationCount.get(Orientation.CCW)!
                this.orientationCount.set(Orientation.CCW, o + 1);
            } else if (this.cameFrom == "S") {
                let o = this.orientationCount.get(Orientation.CW)!
                this.orientationCount.set(Orientation.CW, o + 1);
            }
            this.currentCol += 1;
            this.cameFrom = "W";
        } else if (nextDirection == "W") {
            if (this.cameFrom == "S") {
                let o = this.orientationCount.get(Orientation.CCW)!
                this.orientationCount.set(Orientation.CCW, o + 1);
            } else if (this.cameFrom == "N") {
                let o = this.orientationCount.get(Orientation.CW)!
                this.orientationCount.set(Orientation.CW, o + 1);
            }
            this.currentCol -= 1;
            this.cameFrom = "E";
        }

        this.steps += 1;

        let newPipe = this.pipeAt(this.currentRow, this.currentCol);

        if (newPipe.value != "S" && (this.steps < newPipe.minSteps || newPipe.minSteps == -1)) {
            newPipe.minSteps = this.steps;
        }

        // update insides and outsides

        if (this.orientation == Orientation.Straight) { return; } // not yet solved for

        var insides: Pipe[] = [];
        var outsides: Pipe[] = [];
        var defaultDirection = ".";

        if (p.value == "|") {
            defaultDirection = "N";
            insides = [
                this.pipeAt(p.row, p.col+1)
            ]
            outsides = [
                this.pipeAt(p.row, p.col-1)
            ]
        }
        if (p.value == "-") {
            defaultDirection = "W";
            insides = [
                this.pipeAt(p.row-1, p.col)
            ]
            outsides = [
                this.pipeAt(p.row+1, p.col)
            ]
        }
        if (p.value == "F") {
            defaultDirection = "E";
            insides = [
                this.pipeAt(p.row+1, p.col+1)
            ]
            outsides = [
                this.pipeAt(p.row-1, p.col),
                this.pipeAt(p.row, p.col-1),
                this.pipeAt(p.row-1, p.col-1)
            ]
        }
        if (p.value == "7") {
            defaultDirection = "S";
            insides = [
                this.pipeAt(p.row+1, p.col-1)
            ]
            outsides = [
                this.pipeAt(p.row-1, p.col),
                this.pipeAt(p.row-1, p.col+1),
                this.pipeAt(p.row, p.col+1)
            ]
        }
        if (p.value == "L") {
            defaultDirection = "N";
            insides = [
                this.pipeAt(p.row-1, p.col+1)
            ]
            outsides = [
                this.pipeAt(p.row, p.col-1),
                this.pipeAt(p.row+1, p.col-1),
                this.pipeAt(p.row+1, p.col),
            ]
        }
        if (p.value == "J") {
            defaultDirection = "W";
            insides = [
                this.pipeAt(p.row-1, p.col-1)
            ]
            outsides = [
                this.pipeAt(p.row, p.col+1),
                this.pipeAt(p.row+1, p.col+1),
                this.pipeAt(p.row+1, p.col),
            ]
        }

        insides.forEach((x)=>x.insideOutside = 1);
        outsides.forEach((x)=>x.insideOutside = -1);

        if (nextDirection != defaultDirection) {
            insides.forEach((x)=>x.insideOutside *= -1);
            outsides.forEach((x)=>x.insideOutside *= -1);
        }
        if (this.orientation != Orientation.CW) {
            insides.forEach((x)=>x.insideOutside *= -1);
            outsides.forEach((x)=>x.insideOutside *= -1);
        }
    }

    printMaze(valueFunc: (p: Pipe) => string|number) {
        // prints the maze
        let maxRows:number = this.pipes.reduce((max,comp)=>comp.row>max?comp.row:max, 0) + 1;
        let maxCols:number = this.pipes.reduce((max,comp)=>comp.col>max?comp.col:max, 0) + 1;
        console.log(`${maxRows} rows and ${maxCols} cols.`);
        for (var r=0; r<maxRows; r++) {
            let rowString: string = this.pipes
                .filter((x)=>x.row==r)
                .sort((a,b)=>a.col-b.col)
                .map(valueFunc)
                .join("");
            console.log(rowString);
        }
    }
}

class Pipe {

    minSteps: number = -1; // How long it takes to get here from the starting point
    exits: string[];
    orientation: Orientation = Orientation.Straight;
    insideOutside: number = 0; // 1 = inside, -1 = outside
    isMain: boolean = false;
    
    constructor(
        public row: number,
        public col: number,
        public value: string,
    ) {
        if (this.value == "-") {
            this.exits = ["E", "W"];
            this.orientation = Orientation.Straight;
        } else if (this.value == "|") {
            this.exits = ["N", "S"];
            this.orientation = Orientation.Straight;
        } else if (this.value == "L") {
            this.exits = ["N", "E"];
            this.orientation = Orientation.CCW;
        } else if (this.value == "J") {
            this.exits = ["N", "W"];
            this.orientation = Orientation.CW;
        } else if (this.value == "7") {
            this.exits = ["S", "W"];
            this.orientation = Orientation.CCW;
        } else if (this.value == "F") {
            this.exits = ["S", "E"];
            this.orientation = Orientation.CW;
        } else if (this.value == "." || this.value=="+") {
            this.exits = [];
        } else if (this.value == "S") { // TODO: adjust for every input
            // this.exits = ["S", "W"];
            // this.orientation = Orientation.CW;
            this.exits = ["S", "E"];
            this.orientation = Orientation.CCW;
        } else {
            throw new Error(`Unknown tile ${this.value}`);
        }
    }
}

export {};

import * as fs from 'fs';
import * as readline from 'readline';

if (require.main === module) {

    var maze = new Maze();
    var row = -1;


    // Create a readable stream for the file
    const fileStream: fs.ReadStream = fs.createReadStream(file);

    // Create an interface for reading the file line by line
    const rl: readline.Interface = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity // Recognize all instances of CR LF ('\r\n') as a single line break.
    });

    // On Line
    rl.on('line', (line: string) => {

        line = `+${line}+` // padding

        row +=1 ;
        var col = -1;

        for (var s of line) {
            col += 1;
            maze.pipes.push(new Pipe(row, col, s));
        }

    });

    // On End
    rl.on('close', () => {

        // setup starting point
        let s = maze.startingPipe();
        s.exits = [];
        if (maze.pipeAt(s.row, s.col+1).exits.includes("W")) { s.exits.push("E")};
        if (maze.pipeAt(s.row, s.col-1).exits.includes("E")) { s.exits.push("W")};
        if (maze.pipeAt(s.row+1, s.col).exits.includes("N")) { s.exits.push("S")};
        if (maze.pipeAt(s.row-1, s.col).exits.includes("S")) { s.exits.push("N")};
        console.log(`Starting point exits: ${s.exits}`);

        maze.resetMoves();

        while (true) {
            let v = maze.pipeAt(maze.currentRow, maze.currentCol).value;
            maze.nextMove();
            if (maze.pipeAt(maze.currentRow, maze.currentCol).value=="S") {
                break;
            }
        }

        if (maze.orientationCount.get(Orientation.CCW)! > maze.orientationCount.get(Orientation.CW)!) {
            maze.orientation = Orientation.CCW
            console.log(`This maze is CCW!`);
        } else if (maze.orientationCount.get(Orientation.CCW)! < maze.orientationCount.get(Orientation.CW)!) {
            maze.orientation = Orientation.CW
            console.log(`This maze is CW!`);
        } else {
            console.log(`This maze is... straight?`);
        }


        // And again!
        maze.resetMoves();
        while (true) {
            maze.nextMove();
            if (maze.pipeAt(maze.currentRow, maze.currentCol).value=="S") {
                break;
            }
        }

        // And now try to flood
        for (
            let pipes = maze.pipes.filter((x)=>x.insideOutside==0 && x.isMain == false && x.value != "+");
            pipes.length > 0;
            pipes = maze.pipes.filter((x)=>x.insideOutside==0 && x.isMain == false && x.value != "+")
            ) {
                for (let p of pipes) {
                    p.insideOutside = [
                        maze.pipeAt(p.row-1, p.col),
                        maze.pipeAt(p.row, p.col-1),
                        maze.pipeAt(p.row+1, p.col),
                        maze.pipeAt(p.row, p.col+1),
                    ].map((x)=>x.insideOutside)
                    .filter((x)=>x!=0)[0]
                }
        }

        maze.printMaze((x)=>{
            // return `${x.value}(${x.minSteps})`
            if (x.isMain) { return x.value; }
            if (x.insideOutside == -1) { return "O"; }
            if (x.insideOutside == 1) { return "I"; }
            return x.value;
        });

        console.log(maze.steps);


        let maxSteps:number = maze.pipes.reduce((max,comp)=>comp.minSteps>max?comp.minSteps:max, 0);
        console.log(`Max: ${maxSteps}`);

        let numberInside = maze.pipes.filter((x)=>x.insideOutside==1 && x.isMain==false).length;
        console.log(`Number Inside: ${numberInside}`);
    
    });

    // On error
    fileStream.on('error', (error: NodeJS.ErrnoException) => {
    console.error(`Error reading the file: ${error.message}`);
    });

}