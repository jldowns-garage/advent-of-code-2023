class SeedRange {
    min: number;
    max: number;

    constructor(min: number, max: number) {
        this.min = min;
        this.max = max;
    }

    includes(n: number): boolean {
        return (n <= this.max && n >= this.min);
    }
}

class Mapping {
    destRangeStart: number;
    destRangeEnd: number;
    sourceRangeStart: number;
    sourceRangeEnd: number;
    rangeLength: number;

    constructor(destRangeStart: number, sourceRangeStart: number, rangeLength: number) {
        this.destRangeStart = destRangeStart;
        this.sourceRangeStart = sourceRangeStart;
        this.rangeLength = rangeLength;
        this.destRangeEnd = destRangeStart + rangeLength - 1;
        this.sourceRangeEnd = sourceRangeStart + rangeLength - 1;
    }

}

class Lookup {
    mappings: Mapping[] = []
    nextLookup: Lookup | null = null;
    previousLookup: Lookup | null = null;
    name: string;

    destFromSource(source: number): number {
        let correctMaps = this.mappings.filter((x)=>{
            return source >= x.sourceRangeStart && source <= x.sourceRangeEnd;
        })
        if (correctMaps.length == 0) {
            return source;
        } else if (correctMaps.length == 1) {
            let cm = correctMaps[0];
            return source - cm.sourceRangeStart + cm.destRangeStart;
        } else {
            throw new Error(`${this.name} has ${correctMaps.length} mappings for ${source}!`)
        }
    }

    sourceFromDest(dest: number): number {
        let correctMaps = this.mappings.filter((x)=>{
            return dest >= x.destRangeStart && dest <= x.destRangeEnd;
        })
        if (correctMaps.length == 0) {
            return dest;
        } else if (correctMaps.length == 1) {
            let cm = correctMaps[0];
            return dest - cm.destRangeStart + cm.sourceRangeStart;
        } else {
            throw new Error(`${this.name} has ${correctMaps.length} mappings for ${dest}!`)
        }
    }

    finalDestinationFromSource(source: number, verbose: boolean = false): number {
        let dest = 0;
        dest = this.destFromSource(source)
        if (verbose){console.log(`${this.name} maps ${source} -> ${dest}`)}

        if (this.nextLookup) {
            return this.nextLookup.finalDestinationFromSource(dest, verbose);
        }

        return dest;
    }

    finalSourceFromDestination(dest: number, verbose: boolean = false): number {
        let source = 0;
        source = this.destFromSource(dest)
        if (verbose){console.log(`${this.name} reverse maps ${dest} -> ${source}`)}

        if (this.previousLookup) {
            return this.previousLookup.finalDestinationFromSource(source, verbose);
        }

        return source;
    }

    constructor(name: string) {
        this.name = name;
    }

    
}

/////////////////
// Reading File
/////////////////

import * as fs from 'fs';
import * as readline from 'readline';
const file: string = 'day05/test.txt';

var currentInput = "";
var lookups: Lookup[] = [];
var seedNumbers: number[] = [];
let minLocation = 2**52;

// Create a readable stream for the file
const fileStream: fs.ReadStream = fs.createReadStream(file);

// Create an interface for reading the file line by line
const rl: readline.Interface = readline.createInterface({
  input: fileStream,
  crlfDelay: Infinity // Recognize all instances of CR LF ('\r\n') as a single line break.
});

// On Line
rl.on('line', (line: string) => {
    
    // Keep track of what input we're on
    if (line.slice(0,6) == "seeds:") {
        currentInput = "seeds"
        let matches = line.match(/\d+/g)?.map((x)=>parseInt(x));
        if (matches) {
            seedNumbers = matches;
            console.log(`Seeds: ${seedNumbers}`);
        }
    } else if (line.includes(":")) {
        currentInput = line.trim().replace(":", "");
        let newLookup = new Lookup(currentInput);
        if (lookups.length > 0) { // Pair these lookups
            lookups.slice(-1)[0].nextLookup = newLookup;
            newLookup.previousLookup = lookups.slice(-1)[0];
        }
        lookups.push(newLookup);
    } else {
        let numbers = line.match(/\d+/g)?.map((x)=>parseInt(x));
        if (numbers) {
            lookups[lookups.length-1].mappings.push(new Mapping(numbers[0], numbers[1], numbers[2]))
        }
    }


  });

// On End
rl.on('close', () => {

    var seedRanges: SeedRange[] = [];

    for (var i = 0; i < seedNumbers.length; i+=2) {
        let minSeed = seedNumbers[i];
        let maxSeed = minSeed + seedNumbers[i+1] - 1;

        seedRanges.push(new SeedRange(minSeed, maxSeed));

        // Let's start with the destinations, and work our way up until we find a match.
        let humidityToLocationMap = lookups[lookups.length-1];
        let sortedMaps = humidityToLocationMap.mappings.sort((a,b)=>a.destRangeStart-b.destRangeStart);
        for (var mapping of sortedMaps) {
            let min = mapping.destRangeStart;
            let max = mapping.destRangeEnd;
            console.log(`Attempting locations ${min}-${max}.`)
            for(let l = min; l <= max; l++) {
                let seed = humidityToLocationMap.finalSourceFromDestination(l);
                for (let sr of seedRanges) {
                    if (sr.includes(seed)) {
                        console.log(`Location ${l} matches seed ${seed}!`);
                        return;
                    }
                }
            }
        }

        // let l = lookups[0].finalDestinationFromSource(s, false);

    }

    console.log(`Lowest location: ${minLocation}`);

});

// On error
fileStream.on('error', (error: NodeJS.ErrnoException) => {
  console.error(`Error reading the file: ${error.message}`);
});
