/* spellchecker: disable */

import {LocationNode, RepeatString} from "./day08"

test('Locations Nodes are correctly build from lines.', () => {
    let correctLN = new LocationNode("AAA", "BBB", "CCC");
    let line = "AAA = (BBB, CCC)";
    let testLN = LocationNode.fromLine(line);
    expect(testLN).toEqual(correctLN);
});
