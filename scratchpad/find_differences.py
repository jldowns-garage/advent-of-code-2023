s1 = set()
s2 = set()

with open("input1.txt") as f:
    for line in f:
        s1.add(line.strip())

with open("input2.txt") as f:
    for line in f:
        s2.add(line.strip())


print(s2-s1)