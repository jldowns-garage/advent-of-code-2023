// Day 6!


function totalDistance(timePressed: number, totalTime: number) {
    let timeLeft = totalTime - timePressed;
    // timePressed is the speed
    let distance = timePressed * timeLeft;
    return distance;
}



/////////////////
// Reading File
/////////////////

import * as fs from 'fs';
import * as readline from 'readline';
const file: string = 'day06/input2.txt';

var times: number[] = []
var distances: number[] = [];
var totalResult: number = 1;

// Create a readable stream for the file
const fileStream: fs.ReadStream = fs.createReadStream(file);

// Create an interface for reading the file line by line
const rl: readline.Interface = readline.createInterface({
  input: fileStream,
  crlfDelay: Infinity // Recognize all instances of CR LF ('\r\n') as a single line break.
});

// On Line
rl.on('line', (line: string) => {

    if (line.includes("Time:")) {
        let matches = line.match(/\d+/g);
        let intMatches: number[] = matches? matches.map((x)=>parseInt(x)) : []
        times = times.concat(...intMatches)
    }

    if (line.includes("Distance:")) {
        let matches = line.match(/\d+/g);
        let intMatches: number[] = matches? matches.map((x)=>parseInt(x)) : []
        distances = distances.concat(...intMatches)
    }

});

// On End
rl.on('close', () => {

    console.log(`Times: ${times}`);
    console.log(`Distances: ${distances}`);

    for (let i = 0; i < times.length; i++) {
        let raceTime = times[i];
        let distanceRecord = distances[i];
        console.log(`Race ${i}. Time: ${raceTime}. Current record: ${distanceRecord}`);
        let winConfigs = [...Array(raceTime + 1).keys()]
            .map((x)=> {
                let t = totalDistance(x, raceTime);
                return t;
            })
            .filter((x)=> x > distanceRecord)
            .length

        console.log(`There are ${winConfigs} winning configurations.`);
        totalResult *= winConfigs;
    }

    console.log(`Result: ${totalResult}`)

});

// On error
fileStream.on('error', (error: NodeJS.ErrnoException) => {
  console.error(`Error reading the file: ${error.message}`);
});
